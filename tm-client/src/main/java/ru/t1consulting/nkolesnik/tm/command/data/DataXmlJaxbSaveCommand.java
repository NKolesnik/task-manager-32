package ru.t1consulting.nkolesnik.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataXmlJaxbSaveRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

public final class DataXmlJaxbSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-jaxb-xml";

    @NotNull
    public static final String DESCRIPTION = "Save data to xml file using JAXB.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataXmlJaxb(new DataXmlJaxbSaveRequest());
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
