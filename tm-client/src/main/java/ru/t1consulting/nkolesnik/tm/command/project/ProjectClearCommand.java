package ru.t1consulting.nkolesnik.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectClearRequest;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear project list.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        getProjectEndpoint().clearProject(new ProjectClearRequest());
    }

}
