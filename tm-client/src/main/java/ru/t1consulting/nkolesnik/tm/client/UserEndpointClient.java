package ru.t1consulting.nkolesnik.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.user.*;
import ru.t1consulting.nkolesnik.tm.dto.response.user.*;

@NoArgsConstructor
public class UserEndpointClient extends AbstractEndpointClient implements IUserEndpoint {

    public UserEndpointClient(@NotNull AbstractEndpointClient abstractClient) {
        super(abstractClient);
    }

    public static void main(String[] args) {
        AuthEndpointClient authClient = new AuthEndpointClient();
        authClient.connect();
        authClient.login(new UserLoginRequest("test", "test"));
        UserEndpointClient userClient = new UserEndpointClient(authClient);
        System.out.println(userClient.showProfileUser(new UserProfileRequest()).getUser().getLogin());
        System.out.println(authClient.logout(new UserLogoutRequest()));
        authClient.disconnect();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRegistryResponse registryUser(@NotNull final UserRegistryRequest request) {
        return call(request, UserRegistryResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserRemoveResponse removeUser(@NotNull final UserRemoveRequest request) {
        return call(request, UserRemoveResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUpdateResponse updateUser(@NotNull final UserUpdateRequest request) {
        return call(request, UserUpdateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserProfileResponse showProfileUser(@NotNull final UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserChangePasswordResponse changePassword(@NotNull final UserChangePasswordRequest request) {
        return call(request, UserChangePasswordResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserLockResponse lockUser(@NotNull final UserLockRequest request) {
        return call(request, UserLockResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserUnlockResponse unlockUser(@NotNull final UserUnlockRequest request) {
        return call(request, UserUnlockResponse.class);
    }

}

