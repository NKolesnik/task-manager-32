package ru.t1consulting.nkolesnik.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.project.ProjectCreateRequest;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        getProjectEndpoint().createProject(new ProjectCreateRequest(name, description));
    }

}
