package ru.t1consulting.nkolesnik.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IAuthEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLogoutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLoginResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserLogoutResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient client = new AuthEndpointClient();
        client.connect();
        System.out.println(client.login(new UserLoginRequest("admin", "admin")).getSuccess());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getLogin());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getEmail());
        System.out.println(client.profile(new UserProfileRequest()).getUser().getRole());
        System.out.println(client.logout(new UserLogoutRequest()));
        client.disconnect();
    }

    @Override
    @SneakyThrows
    public UserLoginResponse login(UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @SneakyThrows
    public UserLogoutResponse logout(UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @SneakyThrows
    public UserProfileResponse profile(UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }
}
