package ru.t1consulting.nkolesnik.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.data.DataJsonFasterXmlSaveRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

public final class DataJsonFasterXmlSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-fasterxml-json";

    @NotNull
    public static final String DESCRIPTION = "Save data to JSON file using FasterXML.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataJsonFasterXml(new DataJsonFasterXmlSaveRequest());
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
