package ru.t1consulting.nkolesnik.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserUpdateRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-update-profile";

    @NotNull
    public static final String DESCRIPTION = "Update user info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ENTER FIRST NAME]");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("[ENTER MIDDLE NAME]");
        @NotNull final String middleName = TerminalUtil.nextLine();
        System.out.println("[ENTER LAST NAME]");
        @NotNull final String lastName = TerminalUtil.nextLine();
        getUserEndpoint().updateUser(new UserUpdateRequest(firstName, middleName, lastName));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
