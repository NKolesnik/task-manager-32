package ru.t1consulting.nkolesnik.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.model.ICommand;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    @Setter
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getName();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public String getArgument() {
        return null;
    }

    public abstract void execute();

    @Nullable
    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        String result = "";
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        if (name != null && !name.isEmpty()) {
            result += name + " : ";
        }
        if (argument != null && !argument.isEmpty()) {
            result += argument + " : ";
        }
        if (description != null && !description.isEmpty()) {
            result += description;
        }
        return result;
    }
}
