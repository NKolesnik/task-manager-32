package ru.t1consulting.nkolesnik.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.ISystemEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.system.ServerAboutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.system.ServerVersionRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.system.ServerAboutResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.system.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpoint {

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse aboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(aboutResponse.getEmail());
        System.out.println(aboutResponse.getName());
        @NotNull final ServerVersionResponse versionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(versionResponse.getVersion());
        client.disconnect();


    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
