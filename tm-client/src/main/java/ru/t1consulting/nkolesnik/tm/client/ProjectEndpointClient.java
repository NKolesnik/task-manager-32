package ru.t1consulting.nkolesnik.tm.client;


import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.dto.request.project.*;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserLoginRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.user.UserProfileRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.project.*;
import ru.t1consulting.nkolesnik.tm.model.Project;

@NoArgsConstructor
public class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final AuthEndpointClient authClient = new AuthEndpointClient();
        authClient.connect();
        System.out.println(authClient.login(new UserLoginRequest("admin", "admin")).getSuccess());
        String userId = authClient.profile(new UserProfileRequest()).getUser().getId();
        final ProjectEndpointClient projectEndpointClient = new ProjectEndpointClient(authClient);
        Project project = projectEndpointClient.createProject(new ProjectCreateRequest("serverProj", "why")).getProject();
        System.out.println(project);
        System.out.println(projectEndpointClient.listProject(new ProjectListRequest()).getProjects());
        authClient.disconnect();

    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIdResponse completeProjectStatusById(@NotNull ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectCompleteByIndexResponse completeProjectStatusByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull final ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override

    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectListResponse listProject(@NotNull final ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIdResponse showProjectById(@NotNull final ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectShowByIndexResponse showProjectByIndex(@NotNull final ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}