package ru.t1consulting.nkolesnik.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractEndpointClient {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @Nullable
    private Socket socket;

    public AbstractEndpointClient(@NotNull final AbstractEndpointClient client) {
        this.host = client.host;
        this.port = client.port;
        this.socket = client.socket;
    }

    @SneakyThrows
    public void connect() {
        socket = new Socket(host, port);
    }

    @SneakyThrows
    public void disconnect() {
        if (socket == null) return;
        socket.close();
    }

    @NotNull
    @SneakyThrows
    protected Object call(@NotNull Object data) {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }


    protected <T> T call(final Object data, Class<T> clazz) {
        try {
            getObjectOutputStream().writeObject(data);
            @NotNull final Object result = getObjectInputStream().readObject();
            if (result instanceof ApplicationErrorResponse) {
                ApplicationErrorResponse response = (ApplicationErrorResponse) result;
                throw new RuntimeException(response.getMessage());
            }
            return (T) result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @NotNull
    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    @SneakyThrows
    private ObjectInputStream getObjectInputStream(){
        return new ObjectInputStream(getInputStream());
    }

    @Nullable
    @SneakyThrows
    private OutputStream getOutputStream() {
        if (socket == null) return null;
        return socket.getOutputStream();
    }

    @Nullable
    @SneakyThrows
    private InputStream getInputStream() {
        if (socket == null) return null;
        return socket.getInputStream();
    }

}
