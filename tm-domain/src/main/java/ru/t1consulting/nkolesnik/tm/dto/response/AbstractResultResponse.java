package ru.t1consulting.nkolesnik.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class AbstractResultResponse extends AbstractResponse {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    public AbstractResultResponse(@NotNull final Throwable throwable) {
        setSuccess(false);
        setMessage(throwable.getMessage());
    }

}
