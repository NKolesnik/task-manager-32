package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserOwnedRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findById(userId, id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (comparator == null) return findAll(userId);
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) return null;
        if (index == null) return null;
        @Nullable final List<M> models = findAll(userId);
        if (models.isEmpty()) return null;
        return models.get(index);
    }

    @Nullable
    @Override
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()) && id.equals(item.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable final M model = findById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) return null;
        if (index == null) return null;
        @Nullable final M model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @Nullable final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return models.stream()
                .filter(item -> userId.equals(item.getUserId()))
                .count();
    }

}
