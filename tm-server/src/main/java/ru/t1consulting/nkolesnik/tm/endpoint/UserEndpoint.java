package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IUserEndpoint;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.api.service.IUserService;
import ru.t1consulting.nkolesnik.tm.dto.request.user.*;
import ru.t1consulting.nkolesnik.tm.dto.response.user.*;
import ru.t1consulting.nkolesnik.tm.model.User;

public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }


    @NotNull
    @Override
    public UserChangePasswordResponse changePassword(@NotNull UserChangePasswordRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable String password = request.getPassword();
        getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse();

    }

    @NotNull
    @Override
    public UserLockResponse lockUser(@NotNull UserLockRequest request) {
        check(request);
        @Nullable String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    public UserRegistryResponse registryUser(@NotNull UserRegistryRequest request) {
        @Nullable String login = request.getLogin();
        @Nullable String password = request.getPassword();
        @Nullable String email = request.getEmail();
        getUserService().create(login, password, email);
        return new UserRegistryResponse();
    }

    @NotNull
    @Override
    public UserRemoveResponse removeUser(@NotNull UserRemoveRequest request) {
        check(request);
        @Nullable String login = request.getLogin();
        getUserService().removeByLogin(login);
        return new UserRemoveResponse();
    }

    @NotNull
    @Override
    public UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request) {
        check(request);
        @Nullable String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    public UserUpdateResponse updateUser(@NotNull UserUpdateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable String firstName = request.getFirstName();
        @Nullable String lastName = request.getLastName();
        @Nullable String middleName = request.getMiddleName();
        getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateResponse();
    }

    @NotNull
    @Override
    public UserProfileResponse showProfileUser(@NotNull UserProfileRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final User user = getUserService().findById(userId);
        return new UserProfileResponse(user);
    }
}
