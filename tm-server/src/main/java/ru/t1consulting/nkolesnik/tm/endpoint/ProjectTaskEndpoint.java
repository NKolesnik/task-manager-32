package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectTaskEndpoint;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectTaskService;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.task.TaskUnbindFromProjectRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskBindToProjectResponse;
import ru.t1consulting.nkolesnik.tm.dto.response.task.TaskUnbindFromProjectResponse;

public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @Override
    public @NotNull TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse();
    }

    @Override
    public @NotNull TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse();
    }

}
