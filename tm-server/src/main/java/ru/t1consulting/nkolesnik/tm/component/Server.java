package ru.t1consulting.nkolesnik.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.Operation;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;
import ru.t1consulting.nkolesnik.tm.task.AbstractServerTask;
import ru.t1consulting.nkolesnik.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public final class Server {

    @NotNull
    private final Bootstrap bootstrap;
    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();
    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();
    @Getter
    @Nullable
    private ServerSocket serverSocket;

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        serverSocket = new ServerSocket(port);
        submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        dispatcher.registry(reqClass, operation);
    }

    public Object call(@NotNull final AbstractRequest request) {
        return dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        if (serverSocket == null) return;
        serverSocket.close();
        executorService.shutdown();
    }

    public void submit(@NotNull final AbstractServerTask abstractServerTask) {
        executorService.submit(abstractServerTask);
    }

}
