package ru.t1consulting.nkolesnik.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.*;
import ru.t1consulting.nkolesnik.tm.api.repository.IProjectRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserRepository;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.dto.request.data.*;
import ru.t1consulting.nkolesnik.tm.dto.request.project.*;
import ru.t1consulting.nkolesnik.tm.dto.request.system.ServerAboutRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.system.ServerVersionRequest;
import ru.t1consulting.nkolesnik.tm.dto.request.task.*;
import ru.t1consulting.nkolesnik.tm.dto.request.user.*;
import ru.t1consulting.nkolesnik.tm.endpoint.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.repository.ProjectRepository;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;
import ru.t1consulting.nkolesnik.tm.repository.UserRepository;
import ru.t1consulting.nkolesnik.tm.service.*;
import ru.t1consulting.nkolesnik.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);


    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectClearRequest.class, projectEndpoint::clearProject);
        server.registry(ProjectCompleteByIdRequest.class, projectEndpoint::completeProjectStatusById);
        server.registry(ProjectCompleteByIndexRequest.class, projectEndpoint::completeProjectStatusByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectRemoveByIdRequest.class, projectEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectEndpoint::removeProjectByIndex);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectStartByIdRequest.class, projectEndpoint::startProjectById);
        server.registry(ProjectStartByIndexRequest.class, projectEndpoint::startProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCompleteByIdRequest.class, taskEndpoint::completeTaskStatusById);
        server.registry(TaskCompleteByIndexRequest.class, taskEndpoint::completeTaskStatusByIndex);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskStartByIdRequest.class, taskEndpoint::startTaskById);
        server.registry(TaskStartByIndexRequest.class, taskEndpoint::startTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(TaskShowByProjectIdRequest.class, taskEndpoint::showTaskByProjectId);
        server.registry(TaskBindToProjectRequest.class, projectTaskEndpoint::bindTaskToProject);
        server.registry(TaskUnbindFromProjectRequest.class, projectTaskEndpoint::unbindTaskFromProject);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonFasterXmlLoadRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonFasterXmlSaveRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonJaxbLoadRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataJsonJaxbSaveRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataXmlFasterXmlLoadRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlFasterXmlSaveRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataXmlJaxbLoadRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataXmlJaxbSaveRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataYamlFasterXmlLoadRequest.class, domainEndpoint::loadYamlFasterXml);
        server.registry(DataYamlFasterXmlSaveRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(UserChangePasswordRequest.class, userEndpoint::changePassword);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUpdateRequest.class, userEndpoint::updateUser);

    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void run() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        backup.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        backup.stop();
        server.stop();
    }

    private void initDemoData() {
        userService.create("test", "test", "test@test.email.ru", Role.USUAL);
        userService.create("admin", "admin", "admin@admin.email.ru", Role.ADMIN);
        projectService.create(userService.findByLogin("admin").getId(), "admin project", "admin project description");
    }

}


