package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IDomainEndpoint;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.dto.request.data.*;
import ru.t1consulting.nkolesnik.tm.dto.response.data.*;

public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull DataBackupLoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull DataBackupSaveRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull DataBase64LoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull DataBase64SaveRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull DataBinaryLoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull DataBinarySaveRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(@NotNull DataJsonFasterXmlLoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(@NotNull DataJsonFasterXmlSaveRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(@NotNull DataJsonJaxbLoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(@NotNull DataJsonJaxbSaveRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(@NotNull DataXmlFasterXmlLoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(@NotNull DataXmlFasterXmlSaveRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(@NotNull DataXmlJaxbLoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(@NotNull DataXmlJaxbSaveRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataYamlFasterXmlLoadResponse loadYamlFasterXml(@NotNull DataYamlFasterXmlLoadRequest request) {
        return null;
    }

    @NotNull
    @Override
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(@NotNull DataYamlFasterXmlSaveRequest request) {
        return null;
    }

}
