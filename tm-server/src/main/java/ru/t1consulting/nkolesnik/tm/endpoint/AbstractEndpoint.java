package ru.t1consulting.nkolesnik.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.exception.entity.AccessDeniedException;
import ru.t1consulting.nkolesnik.tm.model.User;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(AbstractUserRequest request, Role role) {
        @NotNull final User user = checkUser(request);
        if (role == null) throw new AccessDeniedException();
        @Nullable final Role userRole = user.getRole();
        if (userRole != role) throw new AccessDeniedException();
    }

    protected void check(AbstractUserRequest request) {
        checkUser(request);
    }

    @NotNull
    private User checkUser(AbstractUserRequest request) {
        @Nullable final String userId = request.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }


}
