package ru.t1consulting.nkolesnik.tm.component;

import ru.t1consulting.nkolesnik.tm.api.endpoint.Operation;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractRequest;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public final class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            Class<RQ> reqClass, Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    public Object call(AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }
}
