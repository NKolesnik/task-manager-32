package ru.t1consulting.nkolesnik.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1consulting.nkolesnik.tm.dto.request.project.*;
import ru.t1consulting.nkolesnik.tm.dto.response.project.*;

public interface IProjectEndpoint {

    @NotNull
    ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request);

    @NotNull
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request);

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectCompleteByIdResponse completeProjectStatusById(@NotNull ProjectCompleteByIdRequest request);

    @NotNull
    ProjectCompleteByIndexResponse completeProjectStatusByIndex(@NotNull ProjectCompleteByIndexRequest request);

    @NotNull
    ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request);

    @NotNull
    ProjectListResponse listProject(@NotNull ProjectListRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectShowByIdResponse showProjectById(@NotNull ProjectShowByIdRequest request);

    @NotNull
    ProjectShowByIndexResponse showProjectByIndex(@NotNull ProjectShowByIndexRequest request);

    @NotNull
    ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request);

    @NotNull
    ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request);

    @NotNull
    ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request);

    @NotNull
    ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request);

}
