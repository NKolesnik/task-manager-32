package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.endpoint.IProjectEndpoint;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.IServiceLocator;
import ru.t1consulting.nkolesnik.tm.dto.request.project.*;
import ru.t1consulting.nkolesnik.tm.dto.response.project.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.List;

public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull final ProjectCreateRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    public ProjectListResponse listProject(@NotNull final ProjectListRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @NotNull final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = request.getUserId();
        @Nullable final Status status = request.getStatus();
        @NotNull final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().removeById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex() - 1;
        @Nullable final Project project = getProjectService().removeByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectShowByIdResponse showProjectById(@NotNull final ProjectShowByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().findById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectShowByIndexResponse showProjectByIndex(@NotNull final ProjectShowByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex() - 1;
        @Nullable final Project project = getProjectService().findByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateProjectById(@NotNull final ProjectUpdateByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull final ProjectUpdateByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex() - 1;
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectCompleteByIdResponse completeProjectStatusById(@NotNull ProjectCompleteByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectCompleteByIndexResponse completeProjectStatusByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
        return new ProjectCompleteByIndexResponse(project);
    }

    @NotNull
    @Override
    public ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, Status.NOT_STARTED);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    public ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request) {
        check(request);
        @Nullable final String userId = request.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, Status.NOT_STARTED);
        return new ProjectStartByIndexResponse(project);
    }
}
