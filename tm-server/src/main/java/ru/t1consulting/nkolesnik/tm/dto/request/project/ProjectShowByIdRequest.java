package ru.t1consulting.nkolesnik.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class ProjectShowByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectShowByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}
